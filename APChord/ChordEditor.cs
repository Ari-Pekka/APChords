﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APChord
{
    public class ChordEditor
    {
        private string[] major = { "a ", "a# ", "h ", "c ", "c# ", "d ","d# " , "e ", "f ", "f# ", "g ", "g# " };
        private string[] minor = { "aa", "aa#", "hh", "cc", "cc#", "dd", "dd#", "ee", "ff", "ff#", "gg", "gg#" };
    
        private string note;

        //methods
        public string[] Fixer(string[] chordlines)
        {
        
            //TODO: SHARP CHORDS DOESN'T WORK

            //Check chords line by line
            for (int k = 0; k < chordlines.Length; k++)
            {
                note = chordlines[k];

                for (int i = 0; i < major.Length; i++)
                {
                    // First minor chords per line and replace
                    if (note.ToLower().Contains(minor[i]))
                    {
                        chordlines[k] = note.Replace(minor[i], major[i].Replace(" ", "m"));
                        note = chordlines[k];

                        if (note.ToLower().Contains(major[i].Replace(" ", "m77")))
                        {
                            chordlines[k] = note.Replace(major[i].Replace(" ", "m77"), major[i].Replace(" ", "mΔ7"));
                            note = chordlines[k];
                        }
                      
                    }

                    //then major chords per line and replace
                    if (note.ToLower().Contains(major[i]))
                    {
                        chordlines[k] = note.Replace(major[i], major[i].ToUpper());
                        note = chordlines[k];
                    }

                    // 7 chords: Find from major chords, where space is replaced with 7
                    if (note.ToLower().Contains(major[i].Replace(" ", "77")))
                    {
                         chordlines[k] = note.Replace(major[i].Replace(" ", "77"), major[i].ToUpper().Replace(" ", "Δ7"));
                         note = chordlines[k];                                           
                    }                  
                    
                    chordlines[k] = note.Replace(major[i].Replace(" ", "7"), major[i].ToUpper().Replace(" ", "7"));
                    note = chordlines[k];                
                }

          
            }
            return chordlines;
        }
    }
}

