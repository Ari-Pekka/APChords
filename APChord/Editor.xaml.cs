﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using word = Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace APChord
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class Editor : Window
    {
        //TODO Clean up this mess...
        private int nextPart = 0;
        private string helpChordLine;
        private string helpLyricLine;
        private List<string> lyricParts = new List<string>();
        private string[] chordlines;
        private string[] lyricLines;
        private string empty;
        private string songName;
        private int position;

        //Keys for dictionary -> different setup for different parts
        int contains = 0;
        int partName = 100;
        int partB = 200;       

        //TODO Parts beginnings space??
        private string[] parts = {"A", "B", "C" };

        private List<string> wholeSong = new List<string>();
        word.Application objWord = MainWindow.objWord;
        Parts part = new Parts();
        ChordEditor chordEditor = new ChordEditor();

        //word.Range rng = MainWindow.objWord.ActiveDocument.Range(0, 0);

        public Editor()
        {
            InitializeComponent();
        }

        public void GetArray(List<string> lyrics, string songName)
        {
            this.songName = songName;
            lyricParts = lyrics;
            txb_lyric.Text = lyricParts[nextPart];
            empty = txb_chordAdd.Text;
            //Partnames to combobox
            cbParts.ItemsSource = parts;
            cbParts.SelectedIndex = 0;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
           
            if (nextPart < (lyricParts.Count - 1))
            {
                MakeArrays();
                txb_chordAdd.Text = empty;
                nextPart++;
                txb_lyric.Text = lyricParts[nextPart];              
            }
            else if (nextPart == (lyricParts.Count - 1))
            {
                MakeArrays();
                txb_chordAdd.Text = empty;
                txb_lyric.Text = empty;
                nextPart++;
            }
            else
            {
                MessageBox.Show("No parts left!", "Create song");
            }
        }

        public void MakeArrays()
        {
            //Lyrics to arrays
            helpLyricLine = txb_lyric.Text;
            lyricLines = Regex.Split(helpLyricLine, "\r\n");
            //Array.Resize(ref lyricLines, lyricLines.Length + 1);

            //chords to array
            helpChordLine = txb_chordAdd.Text;

            chordlines = Regex.Split(helpChordLine, "\r\n");

            //remove empty lines from array
            if (chordlines.Length != lyricLines.Length )
            {
                for (int i = lyricLines.Length; i < chordlines.Length; i++)
                {
                    chordlines = chordlines.Where(w => w != chordlines[i]).ToArray();
                }
            }

            //Fix the chords
            chordEditor.Fixer(chordlines);

            //Add all parts to Parts's list -> using identifiers partB, partName and contains
            if (cbParts.SelectedItem as string == "B")
            {
                part.partAdd(partB, ($"     {cbParts.SelectedItem as string}"));
                partB++;
            }
            else
            {
                part.partAdd(partName, ($"     {cbParts.SelectedItem as string}"));
                partName++;
            }

            for (int i = 0; i < lyricLines.Length; i++)
            {
                part.partAdd(contains, chordlines[i] + "\n");
                contains++;
                part.partAdd(contains, lyricLines[i] + "\n");
                contains++;
            }
            contains++;
            part.partAdd(contains, "\n");
            contains++;
        }

        private void btn_tempClose_Click(object sender, RoutedEventArgs e)
        {
            // Instead of this: Go back button?
            this.Close();
        }

        private void btnReady_Click(object sender, RoutedEventArgs e)
        {
            // Write to word file
            part.SongName = songName;
            part.createSong();
        }


        private void txb_chordAdd_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //Writes sharp notes by pressing tab (prevent original action)
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                
                position = txb_chordAdd.SelectionStart;
                txb_chordAdd.Text = txb_chordAdd.Text.Insert(txb_chordAdd.SelectionStart, "#");

                // keep the cursor position
                txb_chordAdd.SelectionStart = position + 1;
            }
        }
    }



    //TODO Createparts moves to Parts, not here
    //private void CreateParts()
    //{
    //    //Bold text start
    //    objWord.Selection.Font.Bold = -1;

    //    if (partNum == 1)
    //    {
    //        objWord.Selection.ParagraphFormat.TabIndent(1);
    //        objWord.Selection.TypeText(parts[partNum] + "\n\r");
    //    }
    //    else
    //    {
    //        objWord.Selection.TypeText(parts[partNum] + "\n\r");
    //    }

    //    //Bold text end
    //    objWord.Selection.Font.Bold = 0;

    //    //write part to doc
    //    //TODO: CREATE CONFIG FILE USING HELPLINE!! (write to txt file)
    //    helpLine = "";
    //        for (int i = 0; i < chordlines.Length; i++)
    //        {              
    //            objWord.Selection.TypeText($"{chordlines[i]}\n{lyricLines[i]}\n");
    //            helpLine += ($"{chordlines[i]}\n{lyricLines[i]}\n");
    //        }

    //    // Part B Indent erase
    //    if (partNum == 1)
    //        objWord.Selection.ParagraphFormat.TabIndent(-1);

    //}
}
