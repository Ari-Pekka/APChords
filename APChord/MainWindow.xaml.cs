﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using word = Microsoft.Office.Interop.Word;
using Microsoft.Win32;

namespace APChord
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Create objWord 
        public static word.Application objWord = new word.Application();

        private void btn_NewFile_Click(object sender, RoutedEventArgs e)
        {
            WordDoc();
            TextAdd textAdd= new TextAdd();
            textAdd.Show();
            this.Close();         
        }

        private void btn_open_Click(object sender, RoutedEventArgs e)
        {
            //Create openfile dialog
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = @"c:\";
            ofd.Multiselect = false;
            ofd.Filter = "Word Document (*.docx) |*.docx|All files (*.*)|*.*";

            //show open file dialog
            Nullable<bool> result = ofd.ShowDialog();
            if (result == true)
            {
                if (ofd.FileName.Contains(".docx"))
                {
                    WordDoc();
                    TextAdd textAdd = new TextAdd();
                    textAdd.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Wrong file format!");
                }
            }
        }

        private void WordDoc ()
        {
            //create application

            objWord.Visible = true;
            objWord.WindowState = word.WdWindowState.wdWindowStateMaximize;

            //Create new file, set margins, paragraphs

            word.Document objDoc = objWord.Documents.Add();

            objWord.ActiveDocument.Paragraphs.LineSpacingRule = word.WdLineSpacing.wdLineSpaceSingle;
            objWord.ActiveDocument.Paragraphs.SpaceAfter = 0;
            objWord.ActiveDocument.PageSetup.TopMargin = 40;
            objWord.ActiveDocument.PageSetup.BottomMargin = 40;
            objWord.ActiveDocument.PageSetup.RightMargin = 60;
            objWord.ActiveDocument.PageSetup.LeftMargin = 60;

            //Font changes
            word.Range rng = objDoc.Paragraphs[1].Range;
            rng.Font.Size = 12;
            rng.Font.Name = "Arial";
          

        }

    }
}
