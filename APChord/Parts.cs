﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using word = Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace APChord
{
    //TODO: Config file? Or is it possible to read straight from word file?

    public class Parts
    {
        word.Application objWord = MainWindow.objWord;

        private string songName;
        private Dictionary<int, string> partContains = new Dictionary<int, string>();
        //private List<string> partContains = new List<string>();

        bool indent = false;

        public string SongName
        {
            get
            {
                return songName;
            }
            set
            {
                songName = value;
            }
        }

        public void partAdd(int a, string part)
        {
            partContains.Add(a, part);
        }

        //Write whole song to Word doc
        public void createSong()
        {
            objWord.Selection.Font.Bold = -1;
            objWord.Selection.TypeText(songName + "\n\r");
            objWord.Selection.Font.Bold = 0;

            foreach (KeyValuePair<int, string> create in partContains)
            {
                if (create.Key >= 100 && create.Key <200)
                {                  
                    if (indent == true)
                    {
                        objWord.Selection.ParagraphFormat.TabIndent(-1);
                        indent = false;
                    }
                    
                    objWord.Selection.Font.Bold = -1;
                    objWord.Selection.TypeText(create.Value + "\n\r");
                    objWord.Selection.Font.Bold = 0;
                }
                else if (create.Key >= 200)
                {
                    if (indent == false)
                    {
                        indent = true;
                        objWord.Selection.ParagraphFormat.TabIndent(1);
                    }
                    objWord.Selection.Font.Bold = -1;                   
                    objWord.Selection.TypeText(create.Value + "\n\r");
                    objWord.Selection.Font.Bold = 0;                    
                }
                else
                {
                    objWord.Selection.TypeText(create.Value);
                }
            }
        }
    }
}

//        objWord.Selection.Font.Bold = -1;

//            if (partNum == 1)
//            {
//                objWord.Selection.ParagraphFormat.TabIndent(1);
//                objWord.Selection.TypeText(parts[partNum] + "\n\r");
//            }
//            else
//            {
//                objWord.Selection.TypeText(parts[partNum] + "\n\r");
//            }

//Bold text end
//objWord.Selection.Font.Bold = 0;

//write part to doc
//            TODO: CREATE CONFIG FILE USING HELPLINE!! (write to txt file)
//            helpLine = "";
//                for (int i = 0; i<chordlines.Length; i++)
//                {              
//                    objWord.Selection.TypeText($"{chordlines[i]}\n{lyricLines[i]}\n");
//                    helpLine += ($"{chordlines[i]}\n{lyricLines[i]}\n");
//                }

//             Part B Indent erase
//            if (partNum == 1)
//                objWord.Selection.ParagraphFormat.TabIndent(-1);
//    }

//}



//foreach (KeyValuePair<string, string> create in partContains)
//{
//    // TODO: Not like this, partnames to list or something
//    if (create.Key.Contains("A") || create.Key.Contains("C"))
//    {
//        objWord.Selection.Font.Bold = -1;
//        objWord.Selection.TypeText(create.Key);
//        objWord.Selection.Font.Bold = 0;
//        objWord.Selection.TypeText(create.Value);
//    }
//    else if (create.Key.Contains("B"))
//    {
//        objWord.Selection.ParagraphFormat.TabIndent(1);
//        objWord.Selection.TypeText(create.Value);
//    }
//    else
//    {
//        objWord.Selection.TypeText(create.Value);
//    }
//}