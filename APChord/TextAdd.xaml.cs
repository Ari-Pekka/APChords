﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using word = Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace APChord
{
    /// <summary>
    /// Interaction logic for TextAdd.xaml
    /// </summary>
    public partial class TextAdd : Window
    {
        public TextAdd()
        {
            InitializeComponent();
        }

        word.Application objWord = MainWindow.objWord;
        private string lyrics;
        private List<string> lyricParts = new List<string>();
        private string songName;

        Editor editor = new Editor();

        private void txb_editor_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {            
            //make space in the beginning
            lyrics = ($"        ");

            lyrics += txb_editor.Text;

            //Add white space beginning of the line
            lyrics = lyrics.Replace("\n", ($"\n        "));
            lyrics = lyrics + "\n\r";

            //add lyric parts in the list
            lyricParts.Add(lyrics);

            //empty text editor
            txb_editor.Text = string.Empty;
        }

        private void btnToEditor_Click(object sender, RoutedEventArgs e)
        {
            //Send songname to Parts
            songName = txb_songName.Text;
            //send lyric list to editor
            editor.GetArray(lyricParts, songName);
            editor.Show();
            // editor.Topmost = true;

            this.Close();
        }

        private void txb_songName_GotFocus(object sender, RoutedEventArgs e)
        {
            //songname text disappears on click
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= txb_songName_GotFocus;

        }
    }
}
