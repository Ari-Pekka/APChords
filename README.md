# APChords - Create chord pages for songs

This is an old hobby project. I would do many things other way now... :).

* This program is made with C# and WPF 
* The program opens Microsoft Word doc and modifies automatically font size, indentations, line spacing, paragraphs and much more
* User interface allows you to add lyrics for the show with drag and drop
* You can add chords straight to your lyrics with keyboard shortcuts

<span style="color:red">*The program is not ready yet*</span>
